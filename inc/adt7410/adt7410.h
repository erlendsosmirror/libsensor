/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ADT7410_H
#define ADT7410_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Warning: The ADT7410 may conflict with the ADS1115
#define ADT7410_ADDRESS_0 0x48
#define ADT7410_ADDRESS_1 0x49
#define ADT7410_ADDRESS_2 0x4A
#define ADT7410_ADDRESS_3 0x4B

// Configuration register definitions
#define ADT7410_CONFIG_1FAULT (0 << 0)
#define ADT7410_CONFIG_2FAULT (1 << 0)
#define ADT7410_CONFIG_3FAULT (2 << 0)
#define ADT7410_CONFIG_4FAULT (3 << 0)
#define ADT7410_CONFIG_CT_LOW (0 << 2)
#define ADT7410_CONFIG_CT_HIGH (1 << 2)
#define ADT7410_CONFIG_INT_LOW (0 << 3)
#define ADT7410_CONFIG_INT_HIGH (1 << 3)
#define ADT7410_CONFIG_INTMODE (0 << 4)
#define ADT7410_CONFIG_COMPODE (1 << 4)
#define ADT7410_CONFIG_MODE_CONTINUOUS (0 << 5)
#define ADT7410_CONFIG_MODE_ONESHOT (1 << 5)
#define ADT7410_CONFIG_MODE_1SPS (2 << 5)
#define ADT7410_CONFIG_MODE_SHUTDOWN (3 << 5)
#define ADT7410_CONFIG_RES_13BIT (0 << 7)
#define ADT7410_CONFIG_RES_16BIT (1 << 7)

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Write the configuration register to <config> to sensor with address
 * <address>. Address can be one of the definitions above. <fd> has to be a
 * valid file descriptor for some i2c bus.
 */
int ADT7410_WriteConfig (int fd, unsigned char config, unsigned char address);

/*
 * Tead temperature register from <address> with file descriptor <fd>.
 */
int ADT7410_ReadTemp (int fd, short *temp, unsigned char address);

/*
 * Convert temperature value to a string. Assumes 13-bit format.
 */
void ADT7410_ToString (short temp, char string[10]);

#endif
