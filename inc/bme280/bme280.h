/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BME280_H
#define BME280_H

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <bmp280/bmp280.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Addresses

// Register indexes
#define BME280_REG_HUM_LSB 0xFE
#define BME280_REG_HUM_MSB 0xFD
#define BME280_REG_CTRL_HUM 0xF2
#define BME280_REG_CALIB26 0xE1

// ID Register values
#define BME280_ID 0x60

// Ctrl hum register values
#define BME280_HUM_OVERSAMPLING_SKIP (0 << 0)
#define BME280_HUM_OVERSAMPLING_1 (1 << 0)
#define BME280_HUM_OVERSAMPLING_2 (2 << 0)
#define BME280_HUM_OVERSAMPLING_4 (3 << 0)
#define BME280_HUM_OVERSAMPLING_8 (4 << 0)
#define BME280_HUM_OVERSAMPLING_16 (5 << 0)

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct BME280_Comp
{
	short H2;
	unsigned char H3;
	short H4;
	short H5;
	char H6;
} BME280_Comp;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

int BME280_SetHumidMode (int fd, unsigned char address, unsigned char mode);
int BME280_GetHumidCompensation (int fd, unsigned char address, BME280_Comp *comp);
int BME280_GetMeasurementsRaw (int fd, unsigned char address, int *measurements);
int BME280_GetMeasurements (int fd, unsigned char address, int *measurements, BMP280_Comp *comp, BME280_Comp *comp2);
unsigned int BME280_CompensateHumidity (int adc, BMP280_Comp *comp, BME280_Comp *comph, int t_fine);

#endif
