/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BMP280_H
#define BMP280_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Addresses
#define BMP280_ADDRESS_0 0x76
#define BMP280_ADDRESS_1 0x77

// Register indexes
#define BMP280_REG_TEMP_CLSB 0xFC
#define BMP280_REG_TEMP_LSB 0xFB
#define BMP280_REG_TEMP_MSB 0xFA
#define BMP280_REG_PRESS_XLSB 0xF9
#define BMP280_REG_PRESS_LSB 0xF8
#define BMP280_REG_PRESS_MSB 0xF7
#define BMP280_REG_CONFIG 0xF5
#define BMP280_REG_CTRL_MEAS 0xF4
#define BMP280_REG_STATUS 0xF3
#define BMP280_REG_RESET 0xE0
#define BMP280_REG_ID 0xD0
#define BMP280_REG_CALIB00 0x88

// ID Register values
#define BMP280_ID 0x58

// Reset register values
#define BMP280_RESET 0xB6

// Status register values
#define BMP280_MEASURING (1 << 3)
#define BMP280_IM_UPDATE (1 << 0)

// Ctrl meas register values
#define BMP280_MODE_SLEEP (0 << 0)
#define BMP280_MODE_FORCED (1 << 0)
#define BMP280_MODE_NORMAL (3 << 0)
#define BMP280_PRESSURE_OVERSAMPLING_SKIP (0 << 2)
#define BMP280_PRESSURE_OVERSAMPLING_1 (1 << 2)
#define BMP280_PRESSURE_OVERSAMPLING_2 (2 << 2)
#define BMP280_PRESSURE_OVERSAMPLING_4 (3 << 2)
#define BMP280_PRESSURE_OVERSAMPLING_8 (4 << 2)
#define BMP280_PRESSURE_OVERSAMPLING_16 (5 << 2)
#define BMP280_TEMP_OVERSAMPLING_SKIP (0 << 5)
#define BMP280_TEMP_OVERSAMPLING_1 (1 << 5)
#define BMP280_TEMP_OVERSAMPLING_2 (2 << 5)
#define BMP280_TEMP_OVERSAMPLING_4 (3 << 5)
#define BMP280_TEMP_OVERSAMPLING_8 (4 << 5)
#define BMP280_TEMP_OVERSAMPLING_16 (5 << 5)

// Config register values

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct BMP280_Comp
{
	unsigned short T1;
	short T2;
	short T3;
	unsigned short P1;
	short P2;
	short P3;
	short P4;
	short P5;
	short P6;
	short P7;
	short P8;
	short P9;
	unsigned char reserved;
	unsigned char H1;
} BMP280_Comp;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

int BMP280_GetID (int fd, unsigned char address, unsigned char *id);
int BMP280_SetMode (int fd, unsigned char address, unsigned char mode);
int BMP280_GetCompensation (int fd, unsigned char address, BMP280_Comp *comp);
int BMP280_GetStatus (int fd, unsigned char address, unsigned char *status);
int BMP280_GetMeasurementsRaw (int fd, unsigned char address, int *measurements);
int BMP280_GetMeasurements (int fd, unsigned char address, int *measurements, BMP280_Comp *comp);
int BMP280_CompensateTemperature (int adc, BMP280_Comp *comp, int *t_fine);
unsigned int BMP280_CompensatePressure (int adc, BMP280_Comp *comp, int t_fine);

#endif
