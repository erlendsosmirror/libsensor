/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INA226_H
#define INA226_H

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Addresses
#define INA226_ADDR_0 0x40
#define INA226_ADDR_1 0x41
#define INA226_ADDR_2 0x42
#define INA226_ADDR_3 0x43

#define INA226_ADDR_4 0x44
#define INA226_ADDR_5 0x45
#define INA226_ADDR_6 0x46
#define INA226_ADDR_7 0x47

#define INA226_ADDR_8 0x48
#define INA226_ADDR_9 0x49
#define INA226_ADDR_10 0x4A
#define INA226_ADDR_11 0x4B

#define INA226_ADDR_12 0x4C
#define INA226_ADDR_13 0x4D
#define INA226_ADDR_14 0x4E
#define INA226_ADDR_15 0x4F

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct INA226_Values
{
	short shuntvoltage;
	short busvoltage;
	short power;
	short current;
} INA226_Values;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int INA226_SetConfig (int fd, unsigned char address, unsigned short reg);
int INA226_SetCalibration (int fd, unsigned char address, unsigned short calib);
int INA226_ReadRegister (int fd, unsigned char address, unsigned char reg, short *value);
int INA226_GetValues (int fd, unsigned char address, INA226_Values *v);

#endif
