###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = libsensor
PROJECT_FILES = $(shell find src -name "*.*")

USR_INC = -I../../lib/libuser/inc

###############################################################################
# Compiler flags, file processing and standard targets
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets

###############################################################################
# Makefile execution
###############################################################################
all: build/$(PROJECT_NAME).a x86lib

build/$(PROJECT_NAME).a: $(OBJECTS)
	@arm-none-eabi-ar rcs $@ $(OBJECTS)

x86lib:
	@mkdir -p build/x86
	@mkdir -p build/x86/adt7410
	@gcc $(USR_INC) -c src/adt7410/adt7410.c -o build/x86/adt7410/adt7410.o
	@ar rcs build/$(PROJECT_NAME)x86.a build/x86/adt7410/adt7410.o
