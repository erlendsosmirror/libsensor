/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <unistd.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int ADT7410_WriteConfig (int fd, unsigned char config, unsigned char address)
{
	unsigned char data[] = {address, 0x03, config};
	return (write (fd, data, sizeof(data)) == 3) ? 0 : -1;
}

int ADT7410_ReadTemp (int fd, short *temp, unsigned char address)
{
	unsigned char data[] = {address, 0, 0};
	int ret = 0;

	ret |= write (fd, data, 2) == 2 ? 0 : -1;
	ret |= read (fd, data, 3) == 3 ? 0 : -1;

	unsigned short temperature = ((unsigned short)data[1] << 8) + (unsigned short)data[2];
	*temp = (short) temperature;
	return ret;
}

void ADT7410_ToString (short temp, char string[10])
{
	if (temp & 0x8000)
	{
		string[0] = '-';
		temp = -temp;
	}
	else
		string[0] = '+';

	temp = temp >> 3;

	char str[16];

	_itoa_withzeroes (temp >> 4, str);
	string[1] = str[8];
	string[2] = str[9];
	string[3] = str[10];
	string[4] = '.';

	_itoa_withzeroes ((temp & 0x0F) * 625, str);
	string[5] = str[7];
	string[6] = str[8];
	string[7] = str[9];
	string[8] = str[10];
	string[9] = 0;
}
