/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <unistd.h>
#include <simpleprint/simpleprint.h>
#include <bme280/bme280.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int BME280_SetHumidMode (int fd, unsigned char address, unsigned char mode)
{
	unsigned char modes[] = {address, BME280_REG_CTRL_HUM, mode};

	if (write (fd, modes, 3) != 3)
		return -1;

	return 0;
}

int BME280_GetHumidCompensation (int fd, unsigned char address, BME280_Comp *comp)
{
	unsigned char getcomp[] = {address, BME280_REG_CALIB26};

	if (write (fd, getcomp, 2) != 2)
		return -1;

	unsigned char co[8];
	co[0] = address;

	if (read (fd, co, sizeof (co)) != sizeof (co))
		return -1;

	comp->H2 = co[1] | (co[2] << 8);
	comp->H3 = co[3];
	comp->H4 = (co[4] << 4) | (co[5] & 0x0F);
	comp->H5 = (co[5] >> 4) | (co[6] << 4);
	comp->H6 = co[7];

	//for (int i = 1; i < 8; i++)
	//	PrintValueHex ("Comp hum", co[i]);

	//PrintValueHex ("H2", comp->H2);
	//PrintValueHex ("H3", comp->H3);
	//PrintValueHex ("H4", comp->H4);
	//PrintValueHex ("H5", comp->H5);
	//PrintValueHex ("H6", comp->H6);

	return 0;
}

int BME280_GetMeasurementsRaw (int fd, unsigned char address, int *measurements)
{
	unsigned char getvals[] = {address, BMP280_REG_PRESS_MSB};

	if (write (fd, getvals, 2) != 2)
		return -1;

	unsigned char val[9];
	val[0] = address;

	if (read (fd, val, 9) != 9)
		return -1;

	//for (int i = 0; i < 8; i++)
	//	PrintValueHex ("Val", val[i+1]);

	int temperature = (val[4] << 12) | (val[5] << 4) | (val[6] >> 4);
	int pressure = (val[1] << 12) | (val[2] << 4) | (val[3] >> 4);
	int humidity = (val[7] << 8) | val[8];

	measurements[0] = temperature;
	measurements[1] = pressure;
	measurements[2] = humidity;
	return 0;
}

int BME280_GetMeasurements (int fd, unsigned char address, int *measurements, BMP280_Comp *comp, BME280_Comp *comp2)
{
	if (BME280_GetMeasurementsRaw (fd, address, measurements))
		return -1;

	//PrintValueHex ("temperature raw", measurements[0]);
	//PrintValueHex ("pressure raw", measurements[1]);
	//PrintValueHex ("humidity raw", measurements[2]);

	int t_fine;
	int temp = BMP280_CompensateTemperature (measurements[0], comp, &t_fine);
	int pres = BMP280_CompensatePressure (measurements[1], comp, t_fine);
	int humi = BME280_CompensateHumidity (measurements[2], comp, comp2, t_fine);

	measurements[0] = temp;
	measurements[1] = pres;
	measurements[2] = humi;
	return 0;
}

// As described in the datasheet
unsigned int BME280_CompensateHumidity (int adc, BMP280_Comp *comp, BME280_Comp *comph, int t_fine)
{
	int var = t_fine - 76800;

	var = (((((adc << 14) - (((int)comph->H4) << 20) - (((int)comph->H5) * var)) +
		16384) >> 15) * (((((((var * ((int)comph->H6)) >> 10) * (((var *
		((int)comph->H3)) >> 11) + ((int)32768))) >> 10) + 2097152) *
		((int)comph->H2) + 8192) >> 14));

	var = (var - (((((var >> 15) * (var >> 15)) >> 7) * ((int) comp->H1)) >> 4));
	var = var < 0 ? 0 : var;
	var = var > 419430400 ? 419430400 : var;
	return (unsigned int) (var >> 12);
}
