/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <unistd.h>
#include <simpleprint/simpleprint.h>
#include <bmp280/bmp280.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int BMP280_GetID (int fd, unsigned char address, unsigned char *id)
{
	unsigned char data[] = {(unsigned char)address, BMP280_REG_ID};

	if (write (fd, data, 2) != 2)
		return -1;

	if (read (fd, data, 2) != 2)
		return -1;

	*id = data[1];
	return 0;
}

int BMP280_SetMode (int fd, unsigned char address, unsigned char mode)
{
	unsigned char modes[] = {address, BMP280_REG_CTRL_MEAS, mode};

	if (write (fd, modes, 3) != 3)
		return -1;

	return 0;
}

int BMP280_GetCompensation (int fd, unsigned char address, BMP280_Comp *comp)
{
	unsigned char getcomp[] = {address, BMP280_REG_CALIB00};

	if (write (fd, getcomp, 2) != 2)
		return -1;

	unsigned char co[13*2+2];
	co[1] = address;

	if (read (fd, co+1, 13*2+1) != 13*2+1)
		return -1;

	BMP280_Comp *c = (BMP280_Comp*) &co[2];

	comp->T1 = c->T1;
	comp->T2 = c->T2;
	comp->T3 = c->T3;
	comp->P1 = c->P1;
	comp->P2 = c->P2;
	comp->P3 = c->P3;
	comp->P4 = c->P4;
	comp->P5 = c->P5;
	comp->P6 = c->P6;
	comp->P7 = c->P7;
	comp->P8 = c->P8;
	comp->P9 = c->P9;
	comp->H1 = c->H1;

	//for (int i = 0; i < 13; i++)
	//	PrintValue ("Comp", ((short*)comp)[i+1]);

	return 0;
}

int BMP280_GetStatus (int fd, unsigned char address, unsigned char *status)
{
	unsigned char getstatus[] = {address, BMP280_REG_STATUS};

	if (write (fd, getstatus, 2) != 2)
		return -1;

	if (read (fd, getstatus, 2) != 2)
		return -1;

	*status = getstatus[1];
	return 0;
}

int BMP280_GetMeasurementsRaw (int fd, unsigned char address, int *measurements)
{
	unsigned char getvals[] = {address, BMP280_REG_PRESS_MSB};

	if (write (fd, getvals, 2) != 2)
		return -1;

	unsigned char val[7];
	val[0] = address;

	if (read (fd, val, 7) != 7)
		return -1;

	//for (int i = 0; i < 6; i++)
	//	PrintValueHex ("Val", val[i+1]);

	int temperature = (val[4] << 12) | (val[5] << 4) | (val[6] >> 4);
	int pressure = (val[1] << 12) | (val[2] << 4) | (val[3] >> 4);

	measurements[0] = temperature;
	measurements[1] = pressure;
	return 0;
}

int BMP280_GetMeasurements (int fd, unsigned char address, int *measurements, BMP280_Comp *comp)
{
	if (BMP280_GetMeasurementsRaw (fd, address, measurements))
		return -1;

	//PrintValueHex ("temperature raw", measurements[0]);
	//PrintValueHex ("pressure raw", measurements[1]);

	int t_fine;
	int temp = BMP280_CompensateTemperature (measurements[0], comp, &t_fine);
	int pres = BMP280_CompensatePressure (measurements[1], comp, t_fine);

	measurements[0] = temp;
	measurements[1] = pres;
	return 0;
}

// As described in the datasheet
int BMP280_CompensateTemperature (int adc, BMP280_Comp *comp, int *t_fine)
{
	int var1 = ((((adc >> 3) - ((int)comp->T1 << 1))) * ((int)comp->T2)) >> 11;
	int var2 = (((((adc >> 4) - ((int)comp->T1)) * ((adc >> 4) - ((int)comp->T1))) >> 12) * ((int)comp->T3)) >> 14;
	t_fine[0] = var1 + var2;
	return (t_fine[0] * 5 + 128) >> 8;
}

// As described in the datasheet
unsigned int BMP280_CompensatePressure (int adc, BMP280_Comp *comp, int t_fine)
{
	long long var1 = ((long long) t_fine) - 128000;
	long long var2 = var1 * var1 * (long long)comp->P6;
	var2 += (var1 * (long long)comp->P5) << 17;
	var2 += ((long long)comp->P4) << 35;
	var1 = ((var1 * var1 * (long long)comp->P3) >> 8) + ((var1 * (long long)comp->P2) << 12);
	var1 = (((((long long)1) << 47) + var1)) * ((long long)comp->P1) >> 33;

	if (!var1)
		return 0;

	long long p = 1048576 - adc;
	p = (((p << 31) - var2) * 3125) / var1;
	var1 = (((long long)comp->P9) * (p >> 13) * (p >> 13)) >> 25;
	var2 = (((long long)comp->P8) * p) >> 19;
	p = ((p + var1 + var2) >> 8) + (((long long)comp->P7) << 4);
	return (unsigned int) p;
}
