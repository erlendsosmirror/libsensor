/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <simpleprint/simpleprint.h>
#include <ds1307/ds1307.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int DS1307_GetClockState (int fd, unsigned char address, char *out)
{
	unsigned char data[2] = {address, 0};

	if (write (fd, data, 2) != 2)
		return -1;

	if (read (fd, data, 2) != 2)
		return -1;

	*out = (char) data[1];
	return 0;
}

int DS1307_SetClockState (int fd, unsigned char address, char state)
{
	unsigned char data[3] = {address, 0, state};

	if (write (fd, data, 3) != 3)
		return -1;

	return 0;
}

int DS1307_GetTimeAsString (int fd, unsigned char address, char *out)
{
	unsigned char data[1+7] = {address, 0};

	if (write (fd, data, 2) != 2)
		return -1;

	if (read (fd, data, 8) != 8)
		return -1;

	// year
	out[0] = '2';
	out[1] = '0';
	out[2] = '0' + (data[7] >> 4);
	out[3] = '0' + (data[7] & 0x0F);
	out[4] = ' ';

	// month
	out[5] = '0' + (data[6] >> 4);
	out[6] = '0' + (data[6] & 0x0F);
	out[7] = ' ';

	// date
	out[8] = '0' + (data[5] >> 4);
	out[9] = '0' + (data[5] & 0x0F);
	out[10] = ' ';

	// day
	out[11] = '0' + (data[4] & 7);
	out[12] = ' ';

	// hours
	out[13] = '0' + ((data[3] >> 4) & 3);
	out[14] = '0' + (data[3] & 0x0F);
	out[15] = ':';

	// minutes
	out[16] = '0' + (data[2] >> 4);
	out[17] = '0' + (data[2] & 0x0F);
	out[18] = ':';

	// seconds
	out[19] = '0' + ((data[1] >> 4) & 7);
	out[20] = '0' + (data[1] & 0x0F);
	out[21] = 0;
	return 0;
}

int DS1307_SetTimeAsString (int fd, unsigned char address, char *out)
{
	unsigned char data[2+7] = {address, 0};

	// Remove ascii
	for (int i = 0; i < 20; i++)
		out[i] = (out[i] - '0') & 0x0F;

	// Seconds
	data[2] = out[20] | (out[19] << 4);

	// Minutes
	data[3] = out[17] | (out[16] << 4);

	// Hours
	data[4] = out[14] | (out[13] << 4);

	// Day
	data[5] = out[11];

	// Date
	data[6] = out[9] | (out[8] << 4);

	// Month
	data[7] = out[6] | (out[5] << 4);

	// Year
	data[8] = out[3] | (out[2] << 4);

	if (write (fd, data, 2+7) != 2+7)
		return -1;

	return 0;
}
