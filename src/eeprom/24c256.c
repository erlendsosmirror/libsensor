/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <simpleprint/simpleprint.h>
#include <eeprom/24c256.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int EEPROM_24C256_Read (int fd, unsigned char device, int address, void *data, int nbytes)
{
	unsigned char a[] = {device, (unsigned char)(address >> 8), (unsigned char)address};

	if (write (fd, a, 3) != 3)
		return -1;

	unsigned char d[65];

	if (nbytes < 0 || nbytes > 64)
		return -1;

	d[0] = device;

	if (read (fd, d, nbytes+1) != nbytes+1)
		return -1;

	for (int i = 0; i < nbytes; i++)
		((unsigned char*)data)[i] = d[i+1];

	return 0;
}

int EEPROM_24C256_Write (int fd, unsigned char device, int address, void *data, int nbytes)
{
	if (nbytes < 0 || nbytes > 64)
		return -1;

	unsigned char d[64+3];

	d[0] = device;
	d[1] = (unsigned char)(address >> 8);
	d[2] = (unsigned char)address;

	for (int i = 0; i < nbytes; i++)
		d[i+3] = ((unsigned char*)data)[i];

	if (write (fd, d, 3+nbytes) != 3+nbytes)
		return -1;

	return 0;
}

int EEPROM_24C256_IsBusy (int fd, unsigned char device, int *busy)
{
	unsigned char a[] = {device};

	if (write (fd, a, 1) != 1)
	{
		if (errno == EIO)
		{
			*busy = 1;
			return 0;
		}

		return -1;
	}

	*busy = 0;
	return 0;
}
