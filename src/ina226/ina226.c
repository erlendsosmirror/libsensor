/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <simpleprint/simpleprint.h>
#include <ina226/ina226.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int INA226_SetConfig (int fd, unsigned char address, unsigned short reg)
{
	unsigned char config[] = {address, 0, reg >> 8, reg};

	if (write (fd, config, 4) != 4)
		return -1;

	return 0;
}

int INA226_SetCalibration (int fd, unsigned char address, unsigned short calib)
{
	unsigned char config[] = {address, 5, calib >> 8, calib};

	if (write (fd, config, 4) != 4)
		return -1;

	return 0;
}

int INA226_ReadRegister (int fd, unsigned char address, unsigned char reg, short *value)
{
	unsigned char config[] = {address, reg, 0};

	if (write (fd, config, 2) != 2)
		return -1;

	if (read (fd, config, 3) != 3)
		return -1;

	*value = (config[1] << 8) | config[2];
	return 0;
}

int INA226_GetValues (int fd, unsigned char address, INA226_Values *v)
{
	if (INA226_ReadRegister (fd, address, 1, &v->shuntvoltage))
		return -1;

	if (INA226_ReadRegister (fd, address, 2, &v->busvoltage))
		return -1;

	if (INA226_ReadRegister (fd, address, 3, &v->power))
		return -1;

	if (INA226_ReadRegister (fd, address, 4, &v->current))
		return -1;

	return 0;
}
